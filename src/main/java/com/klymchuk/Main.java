package com.klymchuk;

import java.util.Scanner;

/**
 * The Main program class.
 */

public class Main {
    /**
     *The program print odd and even number
     * at the specified interval.
     * Use maximum odd and even number
     * to build Fibonacci sequence and
     * count percent of odd and even number
     * in sequence
     *
     * @author Andrii Klymchuk
     * @param args command line values
     */
    public final static void main(final String[] args) {
        int fibonacciAmount = 0;
        Scanner scanner = new Scanner(System.in);

        Interval interval = new Interval();

        interval.initialization();
        System.out.println("Odd number from start to end");
        interval.printOddNumbersFromStartToEnd();
        System.out.println("\nEven number from end to start");
        interval.printEvenNumbersFromEndToStart();
        interval.sumOfOddNumber();
        interval.sumOfEvenNumber();

        System.out.println("Enter fibonacci size");
        fibonacciAmount = scanner.nextInt();

        Fibonacci fibonacci = new Fibonacci();
        fibonacci.buildFibonacci(fibonacciAmount,
                interval.getMaxOddNumber(), interval.getMaxEvenNumber());

        fibonacci.printPercentOddAndEvenNumbers();
    }
}

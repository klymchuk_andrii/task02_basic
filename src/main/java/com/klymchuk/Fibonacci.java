package com.klymchuk;
/**
 * Class Fibonacci build fibonacci sequence.
 * count percent of odd and even number
 * in sequence
 */
public class Fibonacci {

    /**
     * Amount numbers in fibonacci sequence.
     */
    private int fibonacciAmount;

    /**
     * Array of fibonacci numbers.
     */
    private int[] fibonacci;

    /**
     * 100 percent.
     */
    private final int percent = 100;

    /**
     * Constructor of Fibonacci, initializes.
     * Amount numbers in fibonacci sequence.
     */
    Fibonacci() {
        this.fibonacciAmount = 0;
    }

    /**
     * @param fibonacciAmountParam Amount numbers in sequence
     * @param maxOdd               Maximum odd number
     * @param maxEven              Maximum even number
     */
    public final void buildFibonacci(final int fibonacciAmountParam,
                                     final int maxOdd, final int maxEven) {
        this.fibonacciAmount = fibonacciAmountParam;
        fibonacci = new int[fibonacciAmount];

        if (maxOdd > maxEven) {
            fibonacci[0] = maxEven;
            fibonacci[1] = maxOdd;
        } else {
            fibonacci[0] = maxOdd;
            fibonacci[1] = maxEven;
        }

        for (int i = 2; i < fibonacciAmount; i++) {
            fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
        }
    }

    /**
     * @return all even number.
     */
    public final int calculateEvenNumber() {
        int numberOfOddNumbers = 0;
        for (int i = 0; i < fibonacciAmount; i++) {
            if (fibonacci[i] % 2 == 0) {
                numberOfOddNumbers++;
            }
        }
        return numberOfOddNumbers;
    }

    /**
     * Print percent add and even numbers in fibonacci sequence.
     */
    public final void printPercentOddAndEvenNumbers() {
        int numberOfOddNumbers = calculateEvenNumber();

        System.out.println("\n Percent even numbers in fibonacci sequence"
                + ((double) numberOfOddNumbers / fibonacciAmount)
                * percent + "%");
        System.out.println("\n Percent odd numbers in fibonacci sequence"
                + ((double) (fibonacciAmount - numberOfOddNumbers)
                / fibonacciAmount) * percent + "%");
    }
}

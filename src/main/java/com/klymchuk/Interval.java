package com.klymchuk;

import java.util.Scanner;

/**
 * Class Interval print odd and even number
 * at the specified interval.
 * Count number of odd, even number.
 */
public class Interval {
    /**
     * Start of interval.
     */
    private int startOfInterval;
    /**
     * End of interval.
     */
    private int endOfInterval;

    /**
     * Constructor of Interval, initializes
     * start and end of Interval.
     */
    public Interval() {
        startOfInterval = 0;
        endOfInterval = 0;
    }

    /**
     * User enter start and end of interval.
     */
    public final void initialization() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter start of interval");
        startOfInterval = scanner.nextInt();
        System.out.println("Enter end of interval");
        endOfInterval = scanner.nextInt();
    }

    /**
     * @param startOfIntervalParam start of interval
     * @param endOfIntervalParam   end of interval
     */
    public Interval(final int startOfIntervalParam,
                    final int endOfIntervalParam) {
        this.startOfInterval = startOfIntervalParam;
        this.endOfInterval = endOfIntervalParam;
    }

    /**
     * Print all odd numbers from start to end of interval.
     */
    public final void printOddNumbersFromStartToEnd() {
        for (int i = startOfInterval; i <= endOfInterval; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * Print all even numbers from end to start of interval.
     */
    public final void printEvenNumbersFromEndToStart() {
        for (int i = endOfInterval; i >= startOfInterval; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * Print sum of all odd number from interval.
     */
    public final void sumOfOddNumber() {
        int sumOfOddNumbers = 0;
        for (int i = startOfInterval; i <= endOfInterval; i++) {
            if (i % 2 != 0) {
                sumOfOddNumbers += i;
            }
        }
        System.out.println("\n sum of odd numbers" + sumOfOddNumbers + "\n");
    }

    /**
     * Print sum even number from interval.
     */
    public final void sumOfEvenNumber() {
        int sumOfEvenNumbers = 0;
        for (int i = endOfInterval; i >= startOfInterval; i--) {
            if (i % 2 == 0) {
                sumOfEvenNumbers += i;
            }
        }
        System.out.println("\n sum of even numbers" + sumOfEvenNumbers + "\n");
    }

    /**
     * @return maximum odd number
     */
    public final int getMaxOddNumber() {
        if (endOfInterval % 2 == 0) {
            return endOfInterval;
        } else {
            return endOfInterval - 1;
        }
    }

    /**
     * @return maximum even number
     */
    public final int getMaxEvenNumber() {
        if (endOfInterval % 2 == 0) {
            return endOfInterval - 1;
        } else {
            return endOfInterval;
        }
    }

}
